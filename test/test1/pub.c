/*
 * restserver - A smart, efficient, low consumption RESTful application service platform writen in C
 * author	: calvin
 * email	: calvinwilliams@163.com
 *
 * Licensed under the Apache License v2.0, see the file LICENSE in base directory.
 */

#include "test.h"

void TravelUriPathsAndQueries( struct RestServerContext *ctx , char *buf , int buf_size , int *p_buf_len )
{
	char		*method = NULL ;
	int		method_len ;
	char		*uri = NULL ;
	int		uri_len ;
	
	int		uri_paths_count ;
	int		uri_path_index ;
	char		*uri_path = NULL ;
	int		uri_path_len ;
	
	int		queries_count ;
	int		query_index ;
	char		*key = NULL ;
	int		key_len ;
	char		*value = NULL ;
	int		value_len ;
	
	(*p_buf_len) += snprintf( buf+(*p_buf_len) , buf_size-(*p_buf_len)-1 , "--- REST PUBLIC CHECK ---\n" ) ;
	
	method = RSAPIGetHttpMethodPtr( ctx , & method_len ) ;
	(*p_buf_len) += snprintf( buf+(*p_buf_len) , buf_size-(*p_buf_len)-1 , "         method[%d][%.*s]\n" , method_len , method_len,method ) ;
	
	uri = RSAPIGetHttpUriPtr( ctx , & uri_len ) ;
	(*p_buf_len) += snprintf( buf+(*p_buf_len) , buf_size-(*p_buf_len)-1 , "            uri[%d][%.*s]\n" , uri_len , uri_len,uri ) ;
	
	uri_paths_count = RSAPIGetHttpUriPathsCount( ctx ) ;
	(*p_buf_len) += snprintf( buf+(*p_buf_len) , buf_size-(*p_buf_len)-1 , "uri_paths_count[%d]\n" , uri_paths_count ) ;
	for( uri_path_index = 1 ; uri_path_index <= uri_paths_count ; uri_path_index++ )
	{
		uri_path = RSAPIGetHttpUriPathPtr( ctx , uri_path_index , & uri_path_len ) ;
	(*p_buf_len) += snprintf( buf+(*p_buf_len) , buf_size-(*p_buf_len)-1 , "       uri_path[%d][%.*s]\n" , uri_path_index , uri_path_len,uri_path ) ;
	}
	
	queries_count = RSAPIGetHttpUriQueriesCount( ctx ) ;
	(*p_buf_len) += snprintf( buf+(*p_buf_len) , buf_size-(*p_buf_len)-1 , "  queries_count[%d]\n" , queries_count ) ;
	for( query_index = 1 ; query_index <= queries_count ; query_index++ )
	{
		key = RSAPIGetHttpUriQueryKeyPtr( ctx , query_index , & key_len ) ;
		value = RSAPIGetHttpUriQueryValuePtr( ctx , query_index , & value_len ) ;
	(*p_buf_len) += snprintf( buf+(*p_buf_len) , buf_size-(*p_buf_len)-1 , "          query[%d][%.*s][%.*s]\n" , query_index , key_len,key , value_len,value ) ;
	}
	
	return;
}

