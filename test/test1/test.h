#ifndef _H_IN_
#define _H_IN_

#include "restserver_api.h"

#include "LOGC.h"

void TravelUriPathsAndQueries( struct RestServerContext *ctx , char *buf , int buf_size , int *p_buf_len );

int GET_( struct RestServerContext *ctx );
int GET_path1( struct RestServerContext *ctx );
int GET_path1_( struct RestServerContext *ctx );
int GET_path1_path2( struct RestServerContext *ctx );
int GET_path1_path2_( struct RestServerContext *ctx );
int GET_path1_path2_file( struct RestServerContext *ctx );
int GET_path1_n_file( struct RestServerContext *ctx );

int GET_path1_path2_file1__key1_value1( struct RestServerContext *ctx );
int GET_path1_path2_file2__key1_value1__key2_value2( struct RestServerContext *ctx );
int GET_path1_path2_file3__( struct RestServerContext *ctx );
int GET_path1_path2_file4__key1( struct RestServerContext *ctx );
int GET_path1_path2_file5__key1_( struct RestServerContext *ctx );
int GET_path1_path2_file6__key1__( struct RestServerContext *ctx );
int GET_path1_path2_file7__key1___( struct RestServerContext *ctx );

int POST_path1_file( struct RestServerContext *ctx );
int PUT_path1_file( struct RestServerContext *ctx );
int DELETE_path1_file( struct RestServerContext *ctx );

#endif

