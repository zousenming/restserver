/*
 * restserver - A smart, efficient, low consumption RESTful application service platform writen in C
 * author	: calvin
 * email	: calvinwilliams@163.com
 *
 * Licensed under the Apache License v2.0, see the file LICENSE in base directory.
 */

#ifndef _H_IN_
#define _H_IN_

#include "restserver_api.h"

#include "list.h"

#include "fasterjson.h"
#include "fasterhttp.h"
#include "tcpdaemon.h"

#include "IDL_restserver_conf.dsc.h"

#include "LOGC.h"

/*
 * util
 **/

char *StrdupEntireFile( char *pathfilename , int *p_file_len );
int ExpandPathFilename( char *pathfilename , long pathfilename_bufsize );

/*
 * app interface
 **/

int ProcessHttpRequest( struct RestServerContext *ctx );

/*
 * http request parser and response formater
 **/

struct AcceptedSession
{
	int		sock ; /* socket������ */
	struct sockaddr	addr ; /* socket��ַ */
	
	struct HttpEnv	*http_env ; /* HTTP���� */
} ;

struct RestServerHttpUriPath
{
	char	*path ;
	int	path_len ;
} ;

struct RestServerHttpUriQuery
{
	char	*key ;
	int	key_len ;
	char	*value ;
	int	value_len ;
} ;

#define RESTSERVER_URI_PATHS_MAX_COUNT		32
#define RESTSERVER_HTTP_QUERIES_MAX_COUNT	256

struct HttpRequest
{
	char				*http_method ;
	int				http_method_len ;
	
	char				*http_uri ;
	int				http_uri_len ;
	
	struct RestServerHttpUriPath	http_uri_paths[ RESTSERVER_URI_PATHS_MAX_COUNT ] ;
	int				http_uri_paths_count ;
	struct RestServerHttpUriQuery	http_uri_queries[ RESTSERVER_HTTP_QUERIES_MAX_COUNT ] ;
	int				http_uri_queries_count ;
	
	char				*http_host ;
	int				http_host_len ;
	
	char				*http_body ;
	int				http_body_len ;
} ;

struct RestServerContext
{
	restserver_conf			conf ;

	struct AcceptedSession		*p_accepted_session ;
	
	struct HttpRequest		http_req ;
	
	void				*so_handle ;
	funcInitRestApplication		*pfuncInitRestApplication ;
	funcCallRestApplication		*pfuncCallRestApplication ;
	funcCleanRestApplication	*pfuncCleanRestApplication ;
	
	void				*user_data ;
} ;

int ParseHttpToRestServerContext( struct RestServerContext *ctx );

char *GetRestServerContextHttpMethod( struct RestServerContext *ctx , int *p_method_len );
char *GetRestServerContextHttpUri( struct RestServerContext *ctx , int *p_uri_len );

int GetRestServerContextHttpUriPathsCount( struct RestServerContext *ctx );
char *GetRestServerContextHttpUriPathPtr( struct RestServerContext *ctx , int index , int *p_path_len );
int GetRestServerContextHttpUriQueriesCount( struct RestServerContext *ctx );
char *GetRestServerContextHttpUriQueryKeyPtr( struct RestServerContext *ctx , int index , int *p_key_len );
char *GetRestServerContextHttpUriQueryValuePtr( struct RestServerContext *ctx , int index , int *p_value_len );

char *GetRestServerContextHttpRequestBodyPtr( struct RestServerContext *ctx , int *p_body_len );

void SetRestServerContextUserData( struct RestServerContext *ctx , void *user_data );
void *GetRestServerContextUserData( struct RestServerContext *ctx );

int FormatResetServerContextHttpResponse( struct RestServerContext *ctx , char *http_response_body , int http_response_body_len , char *http_header_format , va_list valist );

/*
 * http uri dispatch controler
 **/

struct RestServiceControler *CreateRestServiceControler( struct RestServiceConfig *config_array );
int DispatchRestServiceControler( struct RestServiceControler *controler , struct RestServerContext *ctx );
void DestroyRestServiceControler( struct RestServiceControler *controler );

#endif

