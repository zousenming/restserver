/*
 * restserver - A smart, efficient, low consumption RESTful application service platform writen in C
 * author	: calvin
 * email	: calvinwilliams@163.com
 *
 * Licensed under the Apache License v2.0, see the file LICENSE in base directory.
 */

#include "in.h"

int main()
{
	printf( "sizeof(struct RestServerContext)                          [%ld]\n" , sizeof(struct RestServerContext) );
	printf( "---------------------------------------------------------------\n" );
	printf( "sizeof(struct RestServerContext.conf)                     [%ld]\n" , sizeof(((struct RestServerContext*)0)->conf) );
	printf( "sizeof(struct RestServerContext.p_accepted_session)       [%ld]\n" , sizeof(((struct RestServerContext*)0)->p_accepted_session) );
	printf( "sizeof(struct RestServerContext.http_req)                 [%ld]\n" , sizeof(((struct RestServerContext*)0)->http_req) );
	printf( "sizeof(struct RestServerContext.so_handle)                [%ld]\n" , sizeof(((struct RestServerContext*)0)->so_handle) );
	printf( "sizeof(struct RestServerContext.pfuncInitRestApplication) [%ld]\n" , sizeof(((struct RestServerContext*)0)->pfuncInitRestApplication) );
	printf( "sizeof(struct RestServerContext.pfuncCallRestApplication) [%ld]\n" , sizeof(((struct RestServerContext*)0)->pfuncCallRestApplication) );
	printf( "sizeof(struct RestServerContext.pfuncCleanRestApplication)[%ld]\n" , sizeof(((struct RestServerContext*)0)->pfuncCleanRestApplication) );
	printf( "sizeof(struct RestServerContext.user_data)                [%ld]\n" , sizeof(((struct RestServerContext*)0)->user_data) );
	return 0;
}

